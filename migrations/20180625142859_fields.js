exports.up = function(knex, Promise) {
  return knex.schema.createTable("fields", function(table) {
    table.string("id").primary();
    table.string("type");
    table.string("name");
    table.string("required");
    table.string("fields");
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable("fields");
};
